<?php

use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function($table)
      	{
      	  $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('user_id');
          $table->foreign('user_id')
							->references('id')->on('users')
							->onUpdate('cascade')
							->onDelete('cascade');
          $table->string('title', 32);
          $table->string('content', 320);
          $table->softDeletes();          
          $table->timestamps();
      	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}