<?php

use Illuminate\Database\Migrations\Migration;

class CreatePostTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post_tag', function($table)
      	{
      	  $table->engine = 'InnoDB';
          $table->unsignedInteger('post_id');
          $table->unsignedInteger('tag_id');
          $table->primary(array('post_id', 'tag_id')); 
          $table->foreign('post_id')
							->references('id')->on('posts')
							->onUpdate('cascade')
							->onDelete('cascade');
		  $table->foreign('tag_id')
							->references('id')->on('tags')
							->onUpdate('cascade')
							->onDelete('cascade');
		  $table->softDeletes(); 
          $table->timestamps();
      	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('isTagged');
	}

}