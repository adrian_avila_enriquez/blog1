<?php

use Illuminate\Database\Migrations\Migration;

class CreateCategoryPostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_post', function($table)
      	{
      	  $table->engine = 'InnoDB';
          $table->unsignedInteger('category_id');
          $table->unsignedInteger('post_id');
          $table->primary(array('category_id', 'post_id')); 
          $table->foreign('category_id')
							->references('id')->on('categories')
							->onUpdate('cascade')
							->onDelete('cascade'); 
          $table->foreign('post_id')
							->references('id')->on('posts')
							->onUpdate('cascade')
							->onDelete('cascade');
		  $table->softDeletes();   
          $table->timestamps();
      	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('isCategorized');
	}

}