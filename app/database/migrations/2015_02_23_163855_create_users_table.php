<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
      	{
      	  $table->engine = 'InnoDB';
          $table->increments('id');       
          $table->integer('rol');
          $table->string('name', 32);
          $table->string('username', 32)->unique();
          $table->string('email', 32)->unique();
          $table->string('password', 64)->unique(); 
          $table->string('remember_token', 100)->nullable();
          $table->softDeletes();         
          $table->timestamps();
      	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}