<?php

class CategoryPostTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('category_post')->delete();
        \DB::table('category_post')->insert(array(
          array(
            'category_id'   => '1',
            'post_id'   => '1'          
            ),
          array(
            'category_id'   => '2',
            'post_id'   => '2'
            ),
          array(
            'category_id'   => '3',
            'post_id'   => '3'
            )
          ));
    }
}