<?php

class LoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Login Form
		return View::make('login');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{		
		// User data
		$credentials = array(
			'email'     => Input::get('email'),
			'password'  => Input::get('password')
			); 	

    	// Authentication
		if (Auth::attempt($credentials)) {
			
        	// Auth successfull
			$user = Auth::user();
			
			if($user->rol == 0){		

				return Redirect::route('post.index');

			} else {

				return Redirect::route('administrator.index');
			}
			
		} else {        
        	// Auth failed
			return Redirect::route('login.index');

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	// Log out
	public function logOut()
	{    	
		Auth::logout();
		Session::flush();

		return Redirect::route('post.index');
	}

}