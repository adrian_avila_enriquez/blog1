<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Displays all posts
		$category_options = Category::lists('name', 'id');
		$tag_options = Tag::lists('description', 'id');
		$posts = Post::orderBy('created_at', 'DESC')->with('user', 'categories', 'tags')->get();	

		if (Auth::check()){

			$user = Auth::user();

			if($user->rol == 0){
				// User
				return View::make('user.blog')
				->with('user', $user)
				->with('category_options', $category_options)
				->with('tag_options', $tag_options)
				->with('posts', $posts);
			}
			else{
				// Administrator
				return Redirect::route('administrator.index');
			}		
		}
		else{
			// Visiter
			return View::make('visiter.blog')
			->with('category_options', $category_options)
			->with('tag_options', $tag_options)
			->with('posts', $posts);
		}		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Validation of new post data
		$validator = Post::validate(Input::all());
		$user_id;

		if ($validator->passes()){

			//Validation successfull
			if (Auth::check()){
				$user = Auth::user();
				$user_id = $user->id;
			}
			else
				$user_id = 1;
			
			$post = Post::create(array(
				'title'		=> Input::get('title'),
				'content' 	=> Input::get('content'),
				'user_id' 	=> $user_id
				));

			// Saves post categories
			if (Input::get('categories') != null)
				$post->categories()->sync(Input::get('categories'));

			// Redirects to blog
			return Redirect::route('post.index')
			->with('message', 'Your post has been created!');
		} 
		else{
			// Validation failed
			return Redirect::route('post.index')
			->withErrors($validator);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) 
	{
		$post = Post::find($id);

		if (Auth::check()){

			$user = Auth::user();

			if($user->rol == 0){
				// User
				return View::make('user.post')
				->with('user', $user)
				->with('post', $post);
			}
			else{
				// Administrator
				return View::make('administrator.post')
				->with('user', $user)
				->with('post', $post);
			}		
		}
		else{
			// Visiter
			return View::make('visiter.post')
			->with('post', $post);
		}		


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Tags Post
		$post = Post::find($id);
		
		if (Input::get('tags') != null){
			$post->tags()->sync(Input::get('tags'));
		}
		
		// Redirects to blog - administrator view
		return Redirect::route('administrator.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);
		$post->delete();

		// Redirects to blog - administrator view
		return Redirect::route('administrator.index');
	}

}