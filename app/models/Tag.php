<?php

class Tag extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tags';

	protected $fillable = array('description');

	protected $softDelete = true;
	

	// Relation MANY TO MANY ( M : N )
    public function posts(){

      	return $this->belongsToMany('Post', 'post_tag', 'tag_id', 'post_id');
      								// N Entity, M_N Table, M_id, N_id 
    }

}