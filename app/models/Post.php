<?php

class Post extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';

	protected $fillable = array('user_id', 'title', 'content');

    protected $softDelete = true;
    

	public static function validate($input) {
                
    	$rules = array(
        	        'title'			=> 'Required|Min:2|Max:25',
        	        'content'		=> 'Required|Min:2|Max:100',
        );

        return Validator::make($input, $rules);       
    }

	// Relation ONE to MANY
	public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

	// Relation MANY TO MANY ( M : N )
    public function categories(){

      	return $this->belongsToMany('Category', 'category_post', 'post_id', 'category_id');
      								// N Entity, M_N Table, M_id, N_id 
    }

    public function tags(){

      	return $this->belongsToMany('Tag', 'post_tag', 'post_id', 'tag_id');
    }

}