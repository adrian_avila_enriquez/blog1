<?php

class Category extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	protected $fillable = array('name');

	protected $softDelete = true;
	

	// Relation MANY TO MANY ( M : N )
    public function posts(){

      	return $this->belongsToMany('Post', 'category_post', 'category_id', 'post_id');
      								// N Entity, M_N Table, M_id, N_id 
    }	
}