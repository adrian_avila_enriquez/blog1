<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;


class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = array('rol', 'name', 'username', 'email', 'password');

	protected $softDelete = true;


	// Relation ONE TO MANY
	public function posts(){

		return $this->hasMany('Post');
	}

	public static function validate($input) {

		$rules = array(
			'name'						=> 'Required|Min:3|Max:80',
			'email'     				=> 'Required|Between:3,64|Email|Unique:users',
        	'password'  				=> 'Required|AlphaNum|Between:8,24|confirmed', //Recomended length by OWASP
        	'password_confirmation'		=> 'Required|AlphaNum|Between:8,24'
       		);

		return Validator::make($input, $rules);       
	}

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
    * Get the e-mail address where password reminders are sent.
    *
    * @return string
    */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken() {
		return $this->remember_token;
	}


	public function setRememberToken($value) {
		$this->remember_token = $value;
	}


	public function getRememberTokenName() {
		return 'remember_token';
	}
}