@extends('layouts.visiter')

@section('head')
@parent
@stop 

@section('navbar')
@parent
@stop

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Azur Blog
				<small>The most french blog</small>
			</h1>
		</div>
	</div>
	<!-- /.row -->

	<!-- New entry -->
	<div class="jumbotron">
		{{Form::open(array('route' => array('post.store')))}}
		<fieldset>
			<div class="form-group">
				<label for="title" class="col-lg-2 control-label">Title:</label>
				<div class="col-lg-10">
					<input type="text" name="title" class="form-control" id="title" placeholder="This is my new post">
				</div>
			</div>
			<div class="form-group">
				<label for="content" class="col-lg-2 control-label" >Content:</label>
				<div class="col-lg-10">
					<textarea class="form-control" name="content" rows="3" id="textArea" placeholder="About Azurlingua..."></textarea>
					<span class="help-block">Write a cool post!</span>
				</div>
			</div>

			<div class="form-group">
				<label for="select" class="col-lg-2 control-label">Categories:</label>
				<div class="col-lg-10">
					{{Form::select('categories[]',$category_options,null,array('multiple'=>'multiple', 'class'=>'form-control'))}}
				</div>
			</div>
			<div class="form-group" align="right">
				<div class="col-lg-10 col-lg-offset-2">
					<br>
					{{ Form::reset('Cancel',  array('class' => 'btn btn-default'))}}
					{{ Form::submit('Done',  array('class' => 'btn btn-primary'))}}
				</div>
			</div>
		</fieldset>
		{{Form::close()}} 
	</div>
	<!-- /.row-->


	<!-- Posts -->
	@foreach($posts as $post)
	<div class="row">
		<div class="col-md-7">
			<a href="#">
				<img class="img-responsive" src="http://res.cloudinary.com/azurlingua/image/upload/v1425572228/laravel_logo-700x300_c6mbdy.jpg" alt="">
			</a>
		</div>
		<div class="col-md-5">
			<h2>{{$post->title}}</h2>
			<h4>By {{$post->user->username}}</h4>
			<h5>Posted on {{$post->created_at}}</h5>
			<p>Categories :
				@foreach($post->categories as $category)
				{{$category->name}}
				@endforeach
			</p>
			<p>Tagged as :
				@foreach($post->tags as $tag)
				{{$tag->description}}
				@endforeach
			</p>
		</h5>
		{{ HTML::linkRoute('post.show', 'Read', $post->id, array('class' => 'btn btn-primary btn-lg')) }}
	</div>
</div>
<!-- /.row -->
<hr>
@endforeach 

</div>
<!-- /.container -->
@stop

@section('footer')
@parent
@stop

@section('script')
@parent
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>
<script type="text/javascript"> $('select').select2() </script>
@stop

