@extends('layouts.master')

@section('head')
@parent
@stop

@section('errors')
@if ($errors->any())
<div class="alert alert-dismissible alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>Operation failed!</strong>
	<p> Next problems were found:</p>
	<ul>
		{{ implode('', $errors->all('<li>:message</li>')) }}
	</ul>  
</div>	        
@endif
@show

@section('message')
@if (Session::has('message'))		
<div class="alert alert-dismissible alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{Session::get('message')}}</strong>
</div>
@endif
@show

@section('content')
@parent
@stop

@section('footer')
@parent
@stop

@section('script')
@parent
@stop
