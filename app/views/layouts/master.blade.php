<html lang="en">
<head>
	@section('head')
	<!--Head-->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/css/select2.min.css" rel="stylesheet" />
	{{ HTML::style('css/bootstrap.css')}}
	{{ HTML::style('css/bootstrap-responsive.css')}}
	{{ HTML::style('css/bootstrap.min.css') }}  	
	@show
</head>

<body>

	@section('navbar')
	<!-- Navigation bar-->
	@show

	<div class="container">
		@yield('content')
		<!--Content-->
	</div>

	@section('footer')
	<!-- Footer -->
	<div align="center">
		<footer>
			<div class="row">
				<div class="col-lg-12">
					<p>Copyright &copy; Azur Blog 2015</p>
				</div>
			</div>
			<!-- /.row -->
		</footer>
	</div>
	@show
	
	@section('script')
	<!--Script-->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	{{HTML::script('js/bootstrap.min.js')}}
	@show   
	
</body>
</html>
